// --------
// Test.c++
// --------

// g++ -pedantic -std=c++17 -NDEBUG -O3 -Wall -Weffc++ -Wextra Test.c++ -o Test

#include <algorithm>  // transform
#include <cassert>    // assert
#include <cmath>      // sqrt
#include <ctime>      // clock, clock_t, CLOCKS_PER_SEC
#include <functional> // minus, plus
#include <iomanip>    // setprecision, setw
#include <iostream>   // cout, endl
#include <list>       // list
#include <numeric>    // accumulate, transform_reduce
#include <vector>     // vector

using namespace std;

// 1.227 milliseconds
template <typename RI, typename II, typename T>
T rmse_while (RI b, RI e, II c, T v) {
    if (b == e)
        return v;
    const auto s = e - b;
    while (b != e) {
        const auto d = (*b - *c);
        v += (d * d);
        ++b;
        ++c;}
    return sqrt(v / s);}

// 2.665 milliseconds
template <typename RI, typename II, typename T>
T rmse_accumulate_1 (RI b, RI e, II c, T v) {
    if (b == e)
        return v;
    const auto s  = e - b;
    const auto sq = [] (const T& d) -> T {return d * d;};
    vector<T>  x(s);
    transform(b, e, c, begin(x), minus<T>());
    transform(begin(x), end(x), begin(x), sq);
    v = accumulate(begin(x), end(x), v);
    return sqrt(v / s);}

// 2.429 milliseconds
template <typename RI, typename II, typename T>
T rmse_accumulate_2 (RI b, RI e, II c, T v) {
    if (b == e)
        return v;
    const auto s   = e - b;
    const auto sqd = [] (const T& a, const T& p) -> T {const T d = a - p; return d * d;};
    vector<T>  x(s);
    transform(b, e, c, begin(x), sqd);
    v = accumulate(begin(x), end(x), v);
    return sqrt(v / s);}

// 1.262 milliseconds
template <typename RI, typename II, typename T>
T rmse_accumulate_3 (RI b, RI e, II c, T v) {
    if (b == e)
        return v;
    const auto s    = e - b;
    const auto sqda = [&c] (const T& v, const T& a) -> T {const T d = a - *c; ++c; return v + (d * d);};
    v = accumulate(b, e, v, sqda);
    return sqrt(v / s);}

int main () {
    const vector<int>           ix(1000);
    const list<int>             iy(1000);
    vector<int>::const_iterator b = begin(ix);
    vector<int>::const_iterator e = end(ix);
    list<int>::const_iterator   c = begin(iy);
    const double                v = 0.0;
    const clock_t bc = clock();
    for (int i = 0; i != 1000; ++i) {
        const double w = rmse(b, e, c, v);
    	assert(w == 0.0);}
    const clock_t ec = clock();
    cout << fixed << setprecision(3) << setw(5);
    cout << ((ec - bc) * 1000.0 / CLOCKS_PER_SEC) << " milliseconds" << endl;
    cout << endl;}
