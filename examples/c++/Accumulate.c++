// --------------
// Accumulate.c++
// --------------

// https://en.cppreference.com/w/cpp/algorithm/accumulate

#include <cassert>    // assert
#include <functional> // multiples, plus
#include <iostream>   // cout, endl
#include <numeric>    // accumulate
#include <list>       // list
#include <vector>     // vector

using namespace std;

void test0 () {
    const int  a[] = {2, 3, 4};
    const int* b   = begin(a);
    const int* e   = end(a);
    const int  v   = 0;
    auto       f   = plus<int>();
    assert(accumulate(b, e, v, f) == 9);}

void test1 () {
    const list<int>           x = {2, 3, 4};
    list<int>::const_iterator b = begin(x);
    list<int>::const_iterator e = end(x);
    const int                 v   = 1;
    auto                      f = multiplies<int>();
    assert(accumulate(b, e, v, f) == 24);}

int main () {
    cout << "Accumulate.c++" << endl;
    test0();
    test1();
    cout << "Done." << endl;
    return 0;}
